FROM java:8
WORKDIR /opt/service/doker-sring-boot
ADD target/doker-spring-boot-0.0.1-SNAPSHOT.jar demo1.jar
#设置镜像的时区,避免出现8小时的误差
ENTRYPOINT ["java","-Xms256m","-Xmx512m","-jar","demo1.jar"]