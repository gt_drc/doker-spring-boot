package com.controller;


import com.entity.Visitor;
import com.repository.VisitorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
@Slf4j
public class VisitorController {

    @Autowired
    private VisitorRepository repository;
	
    @RequestMapping("/")
    public String index(HttpServletRequest request) {
        String ip=request.getRemoteAddr();
        Visitor visitor=repository.findByIp(ip);
        if(visitor==null){
            visitor=new Visitor();
            visitor.setIp(ip);
            visitor.setTimes(1);
        }else {
            visitor.setTimes(visitor.getTimes()+1);
        }
        repository.save(visitor);
        log.debug("I have been seen ip "+visitor.getIp()+" "+visitor.getTimes()+" times.");
        log.error("I have been seen ip "+visitor.getIp()+" "+visitor.getTimes()+" times.");
        log.info("I have been seen ip "+visitor.getIp()+" "+visitor.getTimes()+" times.");
        log.warn("I have been seen ip "+visitor.getIp()+" "+visitor.getTimes()+" times.");
        return "I have been seen ip "+visitor.getIp()+" "+visitor.getTimes()+" times.";
    }
}